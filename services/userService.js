const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  createUser(user) {
    this.checkUserUniq({ name: user.name });
    const item = UserRepository.create(user);
    return item;
  }

  getAllUsers() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }
  updateUser(id, user) {
    this.checkUserUniq({ email: user.email });
    const item = UserRepository.update(id, user);
    if (!item) {
      return null;
    }
    return item;
  }
  checkUserUniq(query) {
    const isUserExist = this.search(query);
    if (isUserExist) {
      throw {
        status: 400,
        message: "User with such email already exist",
      };
    }
  }

  deleteUser(id) {
    const item = UserRepository.delete(id);
    if (!item) {
      return null;
    }
  }
}

module.exports = new UserService();
