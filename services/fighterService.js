const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  createFighter(fighter) {
    this.checkFighterUniq({ name: fighter.name });
    const item = FighterRepository.create(fighter);
    return item;
  }

  getAllFighters() {
    const items = FighterRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  updateFighter(id, fighter) {
    this.checkFighterUniq({ name: fighter.name });
    const item = FighterRepository.update(id, fighter);
    if (!item) {
      return null;
    }
    return item;
  }

  deleteFighter(id) {
    const item = FighterRepository.delete(id);
    if (!item) {
      return null;
    }
    return item;
  }

  searchFighter(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  checkFighterUniq(query) {
    const isFighterExist = this.searchFighter(query);
    if (isFighterExist) {
      throw {
        status: 400,
        message: "Fighter with such name already exist",
      };
    }
  }
}

module.exports = new FighterService();
