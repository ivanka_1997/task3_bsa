const { fighter } = require("../models/fighter");
const FighterService = require("../services/fighterService");

const createFighterValid = (req, res, next) => {
  const errorMessage = createFighterValidation(req.body);

  if (errorMessage) {
    res.err = {
      status: 400,
      message: errorMessage,
    };
  }
  next();
};

const updateFighterValid = (req, res, next) => {
  const errorMessage = updateFighterValidation(req.body);

  if (errorMessage) {
    res.err = {
      status: 400,
      message: errorMessage,
    };
  }
  next();
};
function createFighterValidation(newFighter) {
  if (
    Object.keys(newFighter).length === 0 &&
    newFighter.constructor === Object
  ) {
    return "All fields are required.";
  }

  for (let key in newFighter) {
    if (!(key in fighter) || "id" in newFighter) {
      return "There should not be extra fields in the form.";
    }
  }

  if (!newFighter["health"]) {
    newFighter["health"] = fighter["health"];
  }

  for (let key in fighter) {
    if (key !== "id") {
      if (!newFighter[key] || !newFighter[key].toString().length) {
        return "One of the fields is empty. Please fill in all fields.";
      }
    }
  }

  if (
    "power" in newFighter &&
    !(+newFighter.power && +newFighter.power >= 1 && +newFighter.power <= 100)
  ) {
    return "Fighter power should be between 1 and 100.";
  }

  if (
    "defense" in newFighter &&
    !(
      +newFighter.defense &&
      +newFighter.defense >= 1 &&
      +newFighter.defense <= 10
    )
  ) {
    return "Fighter defense should be between 1 and 10.";
  }

  if (
    "health" in newFighter &&
    !(
      +newFighter.health &&
      +newFighter.health >= 1 &&
      +newFighter.health <= 100
    )
  ) {
    return "Fighter health should be between 1 and 100.";
  }
}

function updateFighterValidation(updateFighter) {
  for (let key in updateFighter) {
    if (!(key in fighter) || "id" in updateFighter) {
      return "Update error. There should not be extra fields in the form.";
    }
  }

  for (let key in updateFighter) {
    if (!updateFighter[key].length) {
      return "Update error. One of the fields is empty. Please fill all fields.";
    }
  }

  if (
    "power" in updateFighter &&
    !(
      +updateFighter.power &&
      +updateFighter.power >= 1 &&
      +updateFighter.power <= 100
    )
  ) {
    return "Update error. Fighter power should be between 1 and 100.";
  }

  if (
    "defense" in updateFighter &&
    !(
      +updateFighter.defense &&
      +updateFighter.defense >= 1 &&
      +updateFighter.defense <= 10
    )
  ) {
    return "Update error. Fighter defense should be between 1 and 10.";
  }

  if (
    "health" in updateFighter &&
    !(
      +updateFighter.health &&
      +updateFighter.health >= 1 &&
      +updateFighter.health <= 100
    )
  ) {
    return "Update error. Fighter health should be between 1 and 100.";
  }
}
exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
