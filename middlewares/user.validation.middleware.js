const { user } = require("../models/user");
const UserService = require("../services/userService");

const createUserValid = (req, res, next) => {
  const errorMessage = createUserValidation(req.body);

  if (errorMessage) {
    res.err = {
      status: 400,
      message: errorMessage,
    };
  }
  next();
};

const updateUserValid = (req, res, next) => {
  const errorMessage = updateUserValidation(req.body, req.params.id);

  if (errorMessage) {
    res.err = {
      status: 400,
      message: errorMessage,
    };
  }
  next();
};
function createUserValidation(newUser) {
  if (Object.keys(newUser).length === 0 && newUser.constructor === Object) {
    return "All fields are required.";
  }

  for (let key in newUser) {
    if (!(key in user) || "id" in newUser) {
      return "There should not be extra fields in the form.";
    }
  }

  for (let key in user) {
    if (key !== "id") {
      if (!newUser[key] || !newUser[key].length) {
        return "One of the fields is empty. Please fill it.";
      }
    }
  }

  if ("email" in newUser && !newUser.email.endsWith("@gmail.com")) {
    return "We support only GMAIL service.";
  }

  if (
    "phoneNumber" in newUser &&
    !(
      newUser.phoneNumber.startsWith("+380") &&
      newUser.phoneNumber.length === 13
    )
  ) {
    return "Phone number should be in +380xxxxxxxxx format.";
  }

  if ("password" in newUser && newUser.password.length <= 8) {
    return "Password should contain min 8 symbols.";
  }
}

function updateUserValidation(updateUser, usedId) {
  if (!UserService.search({ id: usedId })) {
    return "Update error. We cannot update a user which does not exist.";
  }

  for (let key in updateUser) {
    if (!(key in user) || "id" in updateUser) {
      return "Update error. There should not be extra fields in the form.";
    }
  }

  for (let key in updateUser) {
    if (!updateUser[key].length) {
      return "Update error. One of the fields is empty. Please fill in all fields.";
    }
  }

  if ("email" in updateUser && !updateUser.email.endsWith("@gmail.com")) {
    return "Update error. We support only GMAIL service.";
  }

  if (
    "phoneNumber" in updateUser &&
    !(
      updateUser.phoneNumber.startsWith("+380") &&
      updateUser.phoneNumber.length === 13
    )
  ) {
    return "Update error. Phone number should be in +380xxxxxxxxx format.";
  }

  if ("password" in updateUser && updateUser.password.length <= 28) {
    return "Update error. Password should contain min 8 symbols.";
  }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
