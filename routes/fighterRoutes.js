const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter

router.post(
  "/",
  createFighterValid,
  function (req, res, next) {
    try {
      if (res.err) {
        return next();
      }
      const fighter = FighterService.createFighter(req.body);
      if (fighter) {
        res.data = fighter;
      } else {
        throw {
          status: 500,
          message: "Sorry, we can't create fighter.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/",
  function (req, res, next) {
    try {
      const allFighters = FighterService.getAllFighters();

      if (allFighters) {
        res.data = allFighters;
      } else {
        throw {
          status: 404,
          message: "Sorry, we can't get all fighters.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  function (req, res, next) {
    try {
      const { id } = req.params;
      const fighter = FighterService.searchFighter({ id });

      if (fighter) {
        res.data = fighter;
      } else {
        throw {
          status: 404,
          message: "Sorry, we can't get current fighter, no such ID in db.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  function (req, res, next) {
    try {
      if (res.err) {
        return next();
      }
      const {
        body,
        params: { id },
      } = req;
      const fighter = FighterService.updateFighter(id, body);

      if (fighter) {
        res.data = fighter;
      } else {
        throw {
          status: 404,
          message: "Sorry, we can't update current fighter.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  function (req, res, next) {
    try {
      const { id } = req.params;
      const fighter = FighterService.deleteFighter(id);

      if (fighter) {
        res.data = fighter;
      } else {
        throw {
          status: 404,
          message: "Sorry, we can't delete current fighter.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
