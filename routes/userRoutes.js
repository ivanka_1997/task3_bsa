const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user

router.post(
  "/",
  createUserValid,
  function (req, res, next) {
    try {
      if (res.err) {
        return next();
      }
      const user = UserService.createUser(req.body);
      if (user) {
        res.data = user;
      } else {
        throw {
          status: 500,
          message: "Sorry, we can't create user.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/",
  function (req, res, next) {
    try {
      const allUsers = UserService.getAllUsers();

      if (allUsers) {
        res.data = allUsers;
      } else {
        throw {
          status: 404,
          message: "Sorry, we can't get all users.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  function (req, res, next) {
    try {
      const { id } = req.params;
      const user = UserService.search({ id });

      if (user) {
        res.data = user;
      } else {
        throw {
          status: 404,
          message: "Sorry, we can't get current user, no such ID in db.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  function (req, res, next) {
    try {
      if (res.err) {
        return next();
      }
      const {
        body,
        params: { id },
      } = req;
      const user = UserService.updateUser(id, body);

      if (user) {
        res.data = user;
      } else {
        throw {
          status: 404,
          message: "Sorry, we can't update current user.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  function (req, res, next) {
    try {
      const { id } = req.params;
      const user = userService.deleteUser(id);

      if (user) {
        res.data = user;
      } else {
        throw {
          status: 404,
          message: "Sorry, we can't delete current user.",
        };
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
